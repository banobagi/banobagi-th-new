$(document).ready(function(){
	var videoState = 'stop';
	$(window).on("load resize", function () {
		var winH = $(window).height();
		$('body').attr('data-mobile',
			(function(){
				var r = ($(window).width() <= 1024) ? true : false;
				return r;
			}())
		);

		$('body').attr('only-mobile',
			(function(){
				var r = ($(window).width() <= 680) ? true : false;
				return r;
			}())
		);
		if ($('body').attr('data-mobile') == 'true'){
			$('.detail-thumbnail__link').off('mouseenter');
			//영상 컨트롤
			intro_player.off('ended');
			videoState = 'stop';
		}else{
			playCheck();
		}
		$('.intro').height(winH);

	});//load,resize
	$(window).on("resize", function(){
		//메인 인트로영상 컨트롤
		if ($('body').attr('data-mobile') == 'false'){
			if(videoState == 'stop'){
				videoReset();
				videoState = 'play';
			}
		}
	});//resize
	$(window).on("scroll", function(){
		var introVidH = $(".intro-video").height();

		if($(window).scrollTop() >= introVidH){
			videoStop();
		}else{
			videoPlay();
		}
	});
	$('.js-korea').on('click',function(e){
		e.preventDefault();
		$('.main-location__map').removeClass('is-active');
		$('.main-location__map.type-korea').addClass('is-active');
		initMap();
	});
	$('.js-thailand').on('click',function(e){
		e.preventDefault();
		$('.main-location__map').removeClass('is-active');
		$('.main-location__map.type-thailand').addClass('is-active');
		initMap2();
	});
});

//인트로 영상 컨트롤
var intro_iframe = $(".intro-video__iframe");
var intro_player = new Vimeo.Player(intro_iframe);
function playCheck(){
	intro_player.on("ended", function() {
		// console.log("ended!");

		var winH = $(window).height();
		$("html, body").animate({
			scrollTop:winH
		},500);
	});
}
function videoReset(){
	intro_player.setCurrentTime(0);
}


//영상플레이
function videoPlay() {
	intro_player.play();
}
//영상 일시정지
function videoStop() {
	intro_player.pause();
}

//메인 검색
$('#main_keyword').focus(function() {
	$(this).parents('.search-keyword').find('.search-keyword__btn').addClass('is-focus');
})
.blur(function() {
	$(this).parents('.search-keyword').find('.search-keyword__btn').removeClass('is-focus');
});

//렛미인
$('.let__link').on('mouseenter',function(){
	$(this).closest('.let__item').addClass('is-active');
}).on('mouseleave',function(){
	$(this).closest('.let__item').removeClass('is-active');
});

//디테일 슬라이드
$('.js-detail-slide').slick({
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    fade: true,
    cssEase: 'linear',
    autoplay: true,
    autoplaySpeed: 4000,
	zIndex:10,
	dotsClass:'detail-slide__dot slide-dot'
});

//디테일리스트 썸네일 오버효과
$('.detail-thumbnail__link').on('mouseenter',function(){
	$(this).closest('.detail-thumbnail__item').addClass('is-hover');
}).on('mouseleave',function(){
	$(this).closest('.detail-thumbnail__item').removeClass('is-hover');
});

 $('.js-list-slide').slick({
    infinite: true,
    speed: 300,
    slidesToShow: 1,
	prevArrow:'.js-slider-prev',
	nextArrow:'.js-slider-next',
    cssEase: 'linear',
	adaptiveHeight: true,
    // autoplay: true,
    autoplaySpeed: 500,
	dots:true,
	accessibility: false,
	responsive:[
		{
			breakpoint: 1024,
			settings: {
				centerMode: true,
				centerPadding: '80px'
			}
		},
		{
			breakpoint: 680,
			settings: {
				centerMode: true,
				centerPadding: '35px'
			}
		}
	]
});
function detailFocus(){
	$('.js-list-slide').off('afterChange').on('afterChange', function(event, slick, currentSlide, nextSlide){
		if ($('body').attr('data-mobile') == 'true'){
			var dataSet = $('.detail-list').offset().top;
			$('html, body').animate({
				scrollTop:dataSet
			},500);
		}
	});
}

$('.js-list-slide').on('beforeChange', function(event, slick, currentSlide, nextSlide){
	var nowNum = nextSlide +1;
	var thumbnailItem = $('.detail-thumbnail__item') ;
	var itemMax = thumbnailItem.length;
	if(nowNum == itemMax ){
		nowNum = 1;
	}
	thumbnailItem.eq(nowNum).addClass('is-active').siblings().removeClass('is-active');
});

$('.js-list-slide').on('swipe', function(event, slick, currentSlide, nextSlide){
	detailFocus();
});

$('.js-thumbnail-move').on('click', function(e){
	e.preventDefault();
	var thumbnailItem = $(this).closest('.detail-thumbnail__item');
	var thumbnailNum = thumbnailItem.index() - 1;
	$('.js-list-slide').slick('slickGoTo', thumbnailNum);
	detailFocus();
});



//스크롤 모션 (real,location)
$(function(){
	$(".js-main-video").each(function(){
		var $this = $(this);
		var n = function() {

			if ($this.offset().top < $(window).scrollTop() + ($(window).height()/1.3) && $this.offset().top > $(window).scrollTop() - $this.height()) {

				$this.addClass('is-active');

				// 윈도우 스크롤 이벤트 함수 n 실행 종료
				// $(window).unbind("scroll", n)
			}else{
				// $this.removeClass('is-active');
			}
		};


		var b = function() {

			// $this 위치값 계산
			if ($this.offset().top < $(window).scrollTop() + ($(window).height()/1.3) && $this.offset().top > $(window).scrollTop() - $this.height()) {

				// 원본 이미지 교체
				$this.addClass('is-active');

				// 윈도우 스크롤 이벤트 함수 n 실행 종료
				// $(window).unbind("scroll", n)
			}else{
				$this.removeClass('is-active');
			}
		};
		// 윈도우 스크롤 이벤트로 함수 n 지속 실행
		$(window).on("scroll", n);
		// $this 위치값 계산
		$(window).on("load", b);
	});
});

// intro slider
var intro_slide = $(".js-intro-slide");
var intro_slider_bar = $(".intro-slide__bar");
var intro_slide_count = $('.intro-slide__counter');
var intro_slide_timer = 5;
var tick, percentTime;

intro_slide.on("init reInit", function(event, slick, currentSlide, nextSlide){
	var slideNum = (currentSlide ? currentSlide : 0) + 1;
    intro_slide_count.text(slideNum + ' / ' + slick.slideCount);

	$(".intro-slide__item.slick-current").find(".intro-slide__tit").addClass("animated fadeInUp");
	$(".intro-slide__item.slick-current").find(".intro-slide__txt").addClass("animated fadeInUp delay-05s");
});
intro_slide.on("beforeChange", function(event, slick, currentSlide, nextSlide){
	startProgressbar();

	var slideNum = nextSlide + 1;
    intro_slide_count.text(slideNum + ' / ' + slick.slideCount);

	$(".intro-slide__tit").removeClass("animated fadeInUp");
	$(".intro-slide__txt").removeClass("animated fadeInUp delay-05s");
});
intro_slide.on("afterChange", function(event, slick, currentSlide){
	$(".intro-slide__item.slick-current").find(".intro-slide__tit").addClass("animated fadeInUp");
	$(".intro-slide__item.slick-current").find(".intro-slide__txt").addClass("animated fadeInUp delay-05s");
});

intro_slide.slick({
	infinite: true,
	arrows: false,
	dots: false,
	autoplay: true,
	speed: 1000,
	slidesToShow: 1,
	slidesToScroll: 1,
	autoplaySpeed: intro_slide_timer*1000,
	fade: true
});

startProgressbar();

function startProgressbar() {
    resetProgressbar();

    percentTime = 0;
    tick = setInterval(interval, 10);
}

function interval() {
  	percentTime += 1 / (intro_slide_timer+0.1);

  	intro_slider_bar.css({
    	height: percentTime+"%"
  	});

  	if(percentTime >= 100){
      	intro_slide.slick('slickNext');

      	startProgressbar();
    }
}

function resetProgressbar() {
    intro_slider_bar.css({
    	height: 0+'%'
    });

    clearTimeout(tick);
}
