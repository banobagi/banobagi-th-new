$(document).ready(function(){
	$(window).on("load resize", function () {
		$('body').attr('data-mobile',
			(function(){
				var r = ($(window).width() <= 1024) ? true : false;
				return r;
			}())
		);
		$('body').attr('only-mobile',
			(function(){
				var r = ($(window).width() <= 680) ? true : false;
				return r;
			}())
		);
		function disableScrollify(toggle){
		    if(toggle){
				$('body').css('overflow','auto');
		        $.scrollify.disable();
		    } else {
				$.scrollify.enable();
		        $.scrollify(settings);
		    }
		}
		$.scrollify(settings);
		if ($('body').attr('only-mobile') == 'true'){
			var detail = $('.onepage').attr('class');
			var detailIs = detail.split(' ');
			if(detailIs[1] =='type-detail'){
				disableScrollify(1);
				var winH = $(window).height();
				$('.detail-section.type-visual').outerHeight(winH);
			}
		} else {
			disableScrollify(0);
		}

		//상담팝업
		$('.js-consultation').click(function(e) {
			if ($('body').attr('data-mobile') == 'false'){
				 $.scrollify.disable();
			 }
		});
		$('.popup-c__close').click(function(e) {
			if ($('body').attr('data-mobile') == 'false'){
				  $.scrollify.enable();
			 }
		});
	});

	var settings = {
		section : ".detail-section",
		interstitialSection:".footer",
		scrollbars:false,
		scrollSpeed: 700,
    	updateHash: true,
		before:function(i,panels) {
			//로딩전
			var ref = panels[i].attr("data-section-name");
			$(".fs-nav .is-active").removeClass("is-active");
  			$(".fs-nav").find("a[href=\"#" + ref + "\"]").addClass("is-active");
			// if(i == 0){
			// 	visualAniPlay();
			// }else{
			// 	visualAniReset();
			// }
		},
		afterRender:function() {
			var pagination = "<ul class=\"fs-nav\">";
			var activeClass = "";
			$(".detail-section").each(function(i) {
				activeClass = "";
				if(i===0) {
				  activeClass = "is-active";
				}
				pagination += "<li class='fs-nav__item'><a class=\"" + activeClass + " fs-nav__link\" href=\"#" + $(this).attr("data-section-name") + "\"></a></li>";
			});
			pagination += "</ul>";
			$(".onepage").append(pagination);
			$(".fs-nav a").on("click",$.scrollify.move);
		}
	};
	function visualAniPlay(){
		$('.detail-section.type-visual').addClass('is-active');
		window.setTimeout(function(){
			TweenMax.fromTo('.detail-visual__tit', 1, {opacity:0,y:40},{opacity:1,y:0,delay:0.3});
		},800);
		window.setTimeout(function(){
			TweenMax.fromTo('.detail-visual__subtit', 1, {opacity:0,y:40},{opacity:1,y:0,delay:0.3});
		},1100);
	}
	function visualAniReset(){
		// $('.detail-visual__tit, .detail-visual__subtit').removeClass('animated fadeInUp');
		$('.detail-section.type-visual').removeClass('is-active');
		$('.detail-visual__tit, .detail-visual__subtit').css('opacity','0');
	}

});

//메뉴오픈시 디테일 스크롤 움직임 막기
$('.mobile-menu').on('click',function(e){
	if ($('body').attr('data-mobile') == 'true' && $('body').attr('only-mobile') == 'false'){
		if($('.header').hasClass('js-open-m')){
			$.scrollify.enable();
		}else{
			$.scrollify.disable();
		}
	}
});

//상단으로 가기
$('.scroll-top').on('click', function(e) {
    e.preventDefault();
    $.scrollify.move("#home");
});
